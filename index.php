<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$hewan_ternak = new animal("Domba");
echo "1. Nama hewan : " .$hewan_ternak->nama_hewan. "<br>";
echo "2. Nama domba : " .$hewan_ternak->nama_domba. "<br>";
echo "3. Jumlah kaki : ".$hewan_ternak->jumlah_kaki."<br>";
echo "4. Berdarah dingin : ".$hewan_ternak->berdarah_dingin. "<br><br>";

$hewan_liar = new ape("Kera");
echo "1. Nama hewan : " .$hewan_liar->nama_hewan. "<br>";
echo "2. Nama Kera : " .$hewan_liar->nama_kera. "<br>";
echo "3. Berdarah dingin : " .$hewan_liar->berdarah_dingin. "<br>";
echo "4. Jumlah kaki : ".$hewan_liar->jumlah_kaki."<br>";
echo $hewan_liar->yell("Auoooooo <br><br>");

$hewan_rawa = new frog("Katak");
echo "1. Nama hewan : " .$hewan_rawa->nama_hewan. "<br>";
echo "2. Nama Katak : " .$hewan_rawa->nama_katak. "<br>";
echo "3. Berdarah dingin : " .$hewan_rawa->berdarah_dingin. "<br>";
echo "4. Jumlah kaki : ".$hewan_rawa->jumlah_kaki."<br>";
echo $hewan_rawa->jump("Lompat, lompat <br><br>");

?>